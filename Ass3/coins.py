def coin_change(A, den):
    c = []
    T = range(A+1)
    c.append(0)
    for a in range(1,A+1):
        csub = float("inf")
        i = len(den)
        # print "a =", a
        while i > 0 and den[i-1] <= a:
            # print "i =", i
            # print "den =", den[i-1]
            if c[a- den[i-1]] < csub:
                csub = c[a-den[i-1]]
                T[a] = den[i-1]
            i -= 1
        c.append(csub + 1)
    print c
    return c[A], T

def main():
    A = 13
    den = [10,6,1]
    c, T = coin_change(A,den)
    a = A
    print T
    print "Num of Coins:",c
    while a > 0:
        print T[a]
        a = a - T[a]


if __name__ == "__main__":
    main()