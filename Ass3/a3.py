# Super Compact version
# n = int(input())
# jobs  = []
# for i in range(n):
#     next = list(map(int,input().split()))
#     jobs.append((next[0], next[1]))
# p = [0, max(jobs[0])]
# for i in range(1,len(jobs)):
#     p.append(max(jobs[i][0] + p[i], jobs[i][1] + p[i-1]))
# print(p[-1])

def max_profit(jobs):
    p = []
    p.append(0)
    p.append(max(jobs[0]))
    for i in range(1,len(jobs)):
        p.append(max(jobs[i][0] + p[i], jobs[i][1] + p[i-1]))
    return p[-1]

def main():
    n = int(input())
    jobs  = []
    for i in range(n):
        next = list(map(int,input().split()))
        jobs.append((next[0], next[1]))
    print(max_profit(jobs))

if __name__ == "__main__":
    main()