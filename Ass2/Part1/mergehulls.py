import copy
#this is the function you should edit. It should take in 2 hulls, hull1 and hull2,
#each of which is a list of length-3 lists of strings, and return the merged hull,
# as a single list of length-3 lists of strings.
def mergehulls(hull1,hull2):
    hull_out = [] # output hull
    hulls = hull1 + hull2 # hulls combined
    endpoints = []
    status = []
    for j in range(len(hulls)): # format data to [x, [x1,x2,y], True/False]
        endpoints.append([hulls[j][0],hulls[j], True])
        endpoints.append([hulls[j][1],hulls[j], False])

    endpoints.sort(key = lambda x : x[0], reverse=False) # sort on x

    # Main algo implements a sweep line
    for p in range(len(endpoints)):
        # print("============================================================")
        # print("Current Point : " + str(endpoints[p][0]))
        if endpoints[p][2]: # if starting point
            start_point = endpoints[p][1][0]
            interval = endpoints[p][1]
            status.append(copy.deepcopy(interval))
            if endpoints[p][1] == max(status, key=lambda x : x[2]):
                if len(status) > 1:
                    hull_out[-1][1] = copy.deepcopy(start_point) # change end point of previous interval
                    if hull_out[-1][1] == hull_out[-1][0]: # remove intervals with the same start and end point
                        hull_out.remove(hull_out[-1])
                    if len(hull_out) > 1:
                        if hull_out[-1][1] == hull_out[-2][1] and hull_out[-1][2] == hull_out[-2][2]:
                            hull_out.remove(hull_out[-1])
                        elif hull_out[-1][0] == hull_out[-2][1] and hull_out[-1][2] == hull_out[-2][2]:
                            hull_out[-2][1] = hull_out[-1][1]
                            hull_out.remove(hull_out[-1])
                hull_out.append(copy.deepcopy(endpoints[p][1]))
        else: # not starting point
            end_point = endpoints[p][1][1]
            interval = endpoints[p][1]
            status.remove(interval)
            if endpoints[p][1][2] == hull_out[-1][2] and len(status) > 0:
                hull_out.append(copy.deepcopy(max(status, key=lambda x : x[2])))
                hull_out[-1][0] = copy.deepcopy(end_point) # change start point of previous interval
                if hull_out[-1][1] == hull_out[-1][0]: # remove intervals with the same start and end point
                    hull_out.remove(hull_out[-1])
                if len(hull_out) > 1:
                    if hull_out[-1][1] == hull_out[-2][1] and hull_out[-1][2] == hull_out[-2][2]:
                        hull_out.remove(hull_out[-1])
                    elif hull_out[-1][0] == hull_out[-2][1] and hull_out[-1][2] == hull_out[-2][2]:
                        hull_out[-2][1] = hull_out[-1][1]
                        hull_out.remove(hull_out[-1])

        if len(hull_out) > 1:
            if hull_out[-1][0] == hull_out[-2][1] and hull_out[-1][2] == hull_out[-2][2]:
                hull_out[-2][1] = hull_out[-1][1]
                hull_out.remove(hull_out[-1])

        # print("Hull:")
        # print(hull_out)
        # print("Status:")
        # print(status)
        # print("============================================================")
    return hull_out

def main():
    #initialise hulls as empty lists
    hull1 = []
    hull2 = []

    #read in hull 1
    while True:
        next = input()
        if next == 'x':
            break
        else:
            hull1.append(list(map(int,next.split())))
            # hull1.append(input().split())

    #read in hull2
    while True:
        try:
            hull2.append(list(map(int,input().split())))
        except(EOFError):
            break

    #call mergehulls and output each line in correct format
    for line in mergehulls(hull1,hull2):
        # print(' '.join(line))
        print(*line, sep=' ')

if __name__ == "__main__":
    main()
