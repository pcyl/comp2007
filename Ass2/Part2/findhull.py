import copy
#start with your solution for 2.1d, not required by highly recommended
def mergehulls(hull1,hull2):
    hull_out = [] # output hull
    hulls = hull1 + hull2 # hulls combined
    endpoints = []
    status = []
    for j in range(len(hulls)): # format data to [x, [x1,x2,y], True/False]
        endpoints.append([hulls[j][0],hulls[j], True])
        endpoints.append([hulls[j][1],hulls[j], False])

    endpoints.sort(key = lambda x : x[0], reverse=False) # sort on x

    # Main algo implements a sweep line
    for p in range(len(endpoints)):
        if endpoints[p][2]: # if starting point
            start_point = endpoints[p][1][0]
            interval = endpoints[p][1]
            status.append(copy.deepcopy(interval))
            if endpoints[p][1] == max(status, key=lambda x : x[2]):
                if len(status) > 1:
                    hull_out[-1][1] = copy.deepcopy(start_point) # change end point of previous interval
                    if hull_out[-1][1] == hull_out[-1][0]: # remove intervals with the same start and end point
                        hull_out.remove(hull_out[-1])
                    if len(hull_out) > 1:
                        if hull_out[-1][1] == hull_out[-2][1] and hull_out[-1][2] == hull_out[-2][2]: # checks for overlapping intervals
                            hull_out.remove(hull_out[-1])
                        elif hull_out[-1][0] == hull_out[-2][1] and hull_out[-1][2] == hull_out[-2][2]: # checks for non-overlapping intervals
                            hull_out[-2][1] = hull_out[-1][1]
                            hull_out.remove(hull_out[-1])
                hull_out.append(copy.deepcopy(endpoints[p][1]))
        else: # not starting point
            end_point = endpoints[p][1][1]
            interval = endpoints[p][1]
            status.remove(interval)
            if endpoints[p][1][2] == hull_out[-1][2] and len(status) > 0: # check whether the height is equal to the height of the last output interval
                hull_out.append(copy.deepcopy(max(status, key=lambda x : x[2])))
                hull_out[-1][0] = copy.deepcopy(end_point) # change start point of previous interval
                if hull_out[-1][1] == hull_out[-1][0]: # remove intervals with the same start and end point
                    hull_out.remove(hull_out[-1])
                if len(hull_out) > 1:
                    if hull_out[-1][1] == hull_out[-2][1] and hull_out[-1][2] == hull_out[-2][2]: # checks for overlapping intervals
                        hull_out.remove(hull_out[-1])
                    elif hull_out[-1][0] == hull_out[-2][1] and hull_out[-1][2] == hull_out[-2][2]: # checks for non-overlapping intervals
                        hull_out[-2][1] = hull_out[-1][1]
                        hull_out.remove(hull_out[-1])

        if len(hull_out) > 1:
            if hull_out[-1][0] == hull_out[-2][1] and hull_out[-1][2] == hull_out[-2][2]: # Final check for non-overlapping intervals
                hull_out[-2][1] = hull_out[-1][1]
                hull_out.remove(hull_out[-1])
    return hull_out

# your solution goes here, reads in a list of segments (left end, right end, height)
# and returns the hull. Divide and conquer solution should call mergehulls
# as a subroutine
def findhull(seglist):
    if len(seglist) == 1:
        return seglist
    hull1 = []
    hull2 = []
    middle = int(len(seglist)/2)
    for i in range(0, middle):
        hull1.append(seglist[i])
    for j in range(middle, len(seglist)):
        hull2.append(seglist[j])
    findhull(hull1)
    findhull(hull2)
    return mergehulls(hull1,hull2)
#IO done here for you

#read in to list of lists of strings, seglist
seglist = []
while True:
    try:
        seglist.append(list(map(int,input().split())))
    except(EOFError):
        break

#print out results
for line in findhull(seglist):
    print(*line, sep=' ')
