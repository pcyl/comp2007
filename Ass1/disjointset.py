class DisjointSet():
    def __init__(self):
        self.parent = dict()
        self.rank = dict()

    def make_set(self,node):
        self.parent[node] = node
        self.rank[node] = 0.0

    def find(self,node):
        if self.parent[node] != node:
            self.parent[node] = self.find(self.parent[node])
        return self.parent[node]

    def union(self,node1,node2):
        root1 = self.find(node1)
        root2 = self.find(node2)
        if root1 != root2:
            if self.rank[root1] > self.rank[root2]:
                self.parent[root2] = root1
            else:
                self.parent[root1] = root2
                if self.rank[root1] == self.rank[root2]:
                    self.rank[root2] += 1.0
