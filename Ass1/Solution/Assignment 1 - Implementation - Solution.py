#! /usr/bin/env python3
from sys import stdin # Reading graph from stdin.
from heapq import heappush, heappop # Heap priority queue used in Prim's algo.

# Sample solution for Assignment 1
# COMP2X07, Semester 2, 2016
#
# Author : Natalie Tridgell
# I apologise in advance if it isn't neat enough or commented enough.
# I attempted to make the code clear for those that don't know python as well. :-)

# There are 2 different approaches to this problem.
# 1. Using a modified version of Kruskal's algorithm.
# 2. Using a modified version of Prims's algorithm.
# Both algorithms run in O( m log(n) ) time

def infeasible():
    raise Exception('There is no feasible solution.')


##################################################################
####### KRUSKAL's ALGORITHM AND UNION FIND DATA STRUCTURE ########
##################################################################


# A basic implementation of the union find data structure.
# Used to keep track of vertex sets in Kruskal's algorithm.
# Notes:
#    - 2 vertices u, v are in the same vertex set if and only if:
#        - there is a path between u and v in the sub graph
#        - find(u) == find(v)
#    - adding an edge is like connecting (union-ing) two vertex sets
#    - if 2 vertices are in the same vertex set,
#        by adding an edge we are forming a cycle (they already had a path)
#
# In Kruskal's we want to add an edge if and only if it doesn't create a cycle.
# This data structure gives us a quick way to check:
#     - if this edge would create a cycle
#     - add an edge(u,v) to the graph - union(u,v)
#
# Example: if we have the graph with 4 vertices, V = {0, 1, 2, 3}:
# 1. Originally, the vertex sets would be: {0}, {1}, {2}, {3}.
# 2. If we add edge (0, 1), the sets become {0, 1}, {2}, {3}.
#       That is, 0 and 1 are connected, so we union their vertex sets.
# 3. If we then add edge (2, 3), the sets become {0, 1}, {2, 3}.
#       That is, 2 and 3 are connected.
# 4. If we add edge (1, 2), the sets become {0, 1, 2, 3}.
#       That is, we have a tree (a single connected component).
# 5. Adding the edge (0, 2) would make a cycle.
#       That is, 0 and 2 are in the same set so there is already a path between them.

class union_find:
    '''
    Union-find data structure (with path compression)
    
    constructor: n The number of elements in a set
                   Same as make_sets(n)
    
    find: u Get the leader of the tree which u is
            part of. It updates parent's of all
            the nodes it traverses to reach the
            leader so that they all point to the
            leader (making subsequent calls faster).
            
    union: u, v
            Joins the two connected components of
            u and v by pointing one of their leaders
            to the other.
    '''
    def __init__(self, n):
        self.parent = list(range(n))
        self.rank = [0]*n

    def find(self, u):
        if self.parent[u] == u:
            return u
        leader_u = self.find(self.parent[u])
        self.parent[u] = leader_u
        return leader_u
    

    def union(self, u, v):
        leader_u = self.find(u)
        leader_v = self.find(v)
        # Don't do anything if u and v are already in the same sets.
        if leader_u == leader_v:
            return

        # Make sure leader_v has rank >= leader_u
        if self.rank[leader_u] > self.rank[leader_v]:
            # Swap leader u and v
            leader_u, leader_v = (leader_v, leader_u)

        # Make leader_u (which has lower rank) the child of leader_v
        self.parent[leader_u] = leader_v
        if self.rank[leader_u] == self.rank[leader_v]:
            self.rank[leader_v] += 1



# Modified Kruskal's algorithm.
# Key ideas:
# 1. Sort edges in increasing weight order
# 2. Add edges to the empty graph [ union(u,v) ] if it doesn't make a cycle and
#    is either:
#        - the first edge being added for an element in A.
#              Note: Must be to an element in V - A. The special case where this
#              does not hold (V = A and n = 2) is handled in the main function.
#        - between 2 elements in V - A.
# Returns the cost of the MST of the edges.
def kruskals(n, edges, is_in_a):
    # used for keeping track of if an element in A already has an edge
    has_edge = [False]*n
    # sort edges in increasing weight order
    edges.sort()
    components = union_find(n)
    cost = 0.0

    # Keep track of the remaining edges. Need to add n - 1 edges to make a tree.
    remaining = n - 1
    for w, u, v in edges:
        if (is_in_a[u] and has_edge[u]) or (is_in_a[v] and has_edge[v]):
            # Don't add an edge to a vertex in A that already has an edge.
            continue
        elif is_in_a[u] and is_in_a[v]:
            # Don't add an edge between 2 vertices in A
            # [NB : unless special case n = 2. Checked in main function.]
            continue
        elif components.find(u) != components.find(v):
            # include the edge (u, v) in the MST
            components.union(u, v)
            has_edge[u] = True
            has_edge[v] = True
            cost += w
            remaining -= 1
            if remaining < 1:
                break

    if remaining > 0:
        infeasible()
    return cost

########################################################################
######## PRIM'S ALGORIHTM USING A PRIORITY QUEUE DATA STRUCTURE ########
########################################################################


# Modified Prims algorithm.
# 1. Put the edges in adjacency list format.
# 2. Add the cost of all the edges from A to V - A.
#      NB: This is the min wt edge from each vertex in A to a vertex in V - A.
# 3. Perform Prims on V - A.
#    [NB: all vertices in V-A only have edges to other vertices in V - A in the
#         adjacency list]
def prims(n, edges, is_in_a):
    # Put edges in adjacency list format.
    # Ignore edges between elements in A.
    adj_list = [[] for i in range(n)]
    for edge in edges:
        w, u, v = edge
        if not (is_in_a[v] and is_in_a[u]):
            adj_list[u].append((w, v))
            adj_list[v].append((w, u))
    
    # Step 2: Sum the min cost for connecting every vertex in A to a vertex in V - A.
    cost = sum([min(adj_list[u])[0] for u, a in enumerate(is_in_a) if a])

    # Prims algorithm on V - A

    # The queue containing (edgewt, vertex) tuples.
    # Note: it should have 1 entry for each vertex, however heapqueue
    # in python doesn't support reducing the edgewt.
    # Instead, we add all edges to unexplored vertices.
    # Giving this part O(mlogm) running time.
    q = []
    # don't add an edge in prim's to anything that has already been explored.
    # Everything in A has already been explored - edges already added in step 2.
    explored = list(is_in_a)

    start_vertex = is_in_a.index(False)
    heappush(q, (0, start_vertex))
    num_explored = sum(is_in_a)
    # add all vertices in V - A.
    while q and num_explored < n:
        edgewt, vertex = heappop(q)
        if explored[vertex]:
            continue
        num_explored += 1
        explored[vertex] = True
        cost += edgewt
        for w, u in adj_list[vertex]:
            # Only add (edgewt, vertex) tuples for unexplored tuples.
            if not explored[u]:
                heappush(q, (w, u))

    if num_explored < n:
        infeasible()

    return cost



def read_input():
    # read first 3 lines from stdin and assign to variables n, m, x
    # as string, string and list of strings (must convert to
    # number types for any numeric work)
    n = int(stdin.readline())
    m = int(stdin.readline())
    a_values = [int(a) for a in stdin.readline().split()]

    # A boolean list storing if a value is in a or not
    is_a = [False]*n
    for a in a_values:
        is_a[a] = True

    # read remaining lines into a list of edges
    edges = []
    for line in stdin:
        split_line = line.strip().split()
        if len(split_line) != 3:
            continue
        u = int(split_line[0])
        v = int(split_line[1])
        w = float(split_line[2])
        edge = (w, u, v)
        edges.append(edge)

    return (n, is_a, edges)


def main():
    # Read in the input.
    n, is_in_a, edges = read_input()
    # Compute number of vertices in A.
    size_of_a = sum(is_in_a)

    cost = 0.0
    # if there aren't enough edges or n > 2 and everything is a leaf,
    # this problem is infeasible.
    if len(edges) < n - 1 or (n > 2 and size_of_a == n):
        infeasible()
        return
    elif size_of_a == 2 and n == 2:
        # This is the only case where we can add an edge between
        # two elements in set A.
        # Must have an edge otherwise handled in previous case.
        # Assumes only 1 edge between (0, 1).
        cost = edges[0][0]
    elif n >= 2: # Note: if n is 1, the cost is 0.
        # Change here to run with prims / kruskals algorithm
        # cost = prims(n, edges, is_in_a)
        cost = kruskals(n, edges, is_in_a)

    # print total weight to 2 decimal places
    print('%.2f' % (cost))

if __name__ == '__main__':
    main()
