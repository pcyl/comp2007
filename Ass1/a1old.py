import operator

def MST(n,m,x,edges):
    total = 0.0
    tree = []
    for edge in edges:
        if len(tree) == n:
            return total
        if (not edge[0] in tree) or (not edge[1] in tree):
            total += float(edge[2])
            tree.append(edge[0])
    return total

def priorityQ(edges):
    temp = []
    for edge in edges:
        edge = edge.split()
        temp.append(edge)
    pq = sorted(temp, key=operator.itemgetter(2))
    return pq

def graph(n,edges):
    g = {}
    for i in range(int(n)):
        g[i] = {}
    for edge in edges:
        edge = edge.split()
        g[edge[0]] = {edge[1] : edge[2]}
    print(g)

def main():
    # read first 3 lines from stdin and assign to variables n, m, x
    # as string, string and list of strings (must convert to
    # number types for any numeric work)
    n = input()
    m = input()
    x = input().split()

    # read remaining lines into a list of edges
    edges = []
    while True:
        try:
            edges.append(input())
        except(EOFError):
            break
    graph(n,edges)
    edges = priorityQ(edges)
    # initialise total weight as 0
    #total = MST(n,m,x,edges)

    # print total weight to 2 decimal places
    print('%.2f' % (total))
    return

if __name__ == "__main__":
    main()
