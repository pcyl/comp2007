from disjointset import DisjointSet

def export_graph(mst_edges,A):
    with open('mygraph.dot', 'w') as f:
        f.write("graph G {\n")
        for e in mst_edges:
            if e[0] in A:
                f.write('%s [color=red];\n' % e[0])
            if e[1] in A:
                f.write('%s [color=red];\n' % e[1])

            f.write('%s -- %s;\n' % (e[0], e[1]))
        f.write("}\n")

'''
Based on Kruskal's Algorithm
'''
def MST(x,graph):
    total = 0.0
    ds = DisjointSet()
    for vertex in graph['vertices']:
        ds.make_set(vertex)
    mst = [] # minimum spanning tree
    connected = [] # x subset vertices that are already connected

    for edge in graph['edges']:
        v1, v2, weight = edge
        if v1 in x and v2 in x:
            continue
        if v1 in connected or v2 in connected:
            continue
        if ds.find(v1) != ds.find(v2):
            if (v1 in x):
                connected.append(v1)
            if (v2 in x):
                connected.append(v2)
            ds.union(v1,v2)
            mst.append(edge)
            total += weight
    #print(mst)
    #export_graph(mst,x)
    return total

def make_graph(n,edges):
    graph = dict()
    graph['vertices'] = []
    graph['edges'] = []
    for i in range(int(n)):
        graph['vertices'].append(str(i))
    for edge in edges:
        e = edge.split()
        e[2] = float(e[2])
        graph['edges'].append(e)
    graph['edges'] = sorted(graph['edges'], key = lambda edge : edge[2]) # sort edges by weight
    return graph

def main():
    # read first 3 lines from stdin and assign to variables n, m, x
    # as string, string and list of strings (must convert to
    # number types for any numeric work)
    n = input()
    m = input()
    x = input().split()

    # read remaining lines into a list of edges
    edges = []
    while True:
        try:
            edges.append(input())
        except(EOFError):
            break
    g = make_graph(n,edges)
    # initialise total weight as 0
    total = MST(x,g)

    # print total weight to 2 decimal places
    print('%.2f' % (total))
    return

if __name__ == "__main__":
    main()
